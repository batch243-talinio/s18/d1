console.log("Hello Zawarudo!");

	/*function printInput(){
		let nickName = prompt("Enter your nick-name: ");
		console.log("Hi " + nickName);
	}
	printInput();*/

	// prompt may be used to gather data but is not ideal

// [Section] Parameters and Arguments

	//consider this function

		function printName(name){ //parameters can also have adefault value [eg. (name = "Mark"); ]
			console.log("My name is " + name);
		}
 		
 		printName("Paeng");

 		printName("Horhe");

 		// with this you can pass data into the function. Function can then use that data is reffered as "name" within the function.

 		// (name) was called parameter.
 		// parameters act as named variable that only exist inside a function.

 		// ("Paeng") is called an argument provided directly into the function.
 		// values passed when invoking a function are called arguments which will be stored as parameters within the function.

 		// variables can also be passed as an argument.

	 		let sampleVariable = "Thomas";
	 		printName(sampleVariable);

		// function arguments cannot be used by a function if there are no parameter provided within the sope.

		function noParams(){
			let params = "No Params";                                                                                                       
		} 		
			noParams("with parent");


		function checkDovivisibility (num){
			let modulo = num%8;        
			console.log("The remainder of " + num + " divided by 8 is: " + modulo);


			let isDivisibleBy8 = modulo === 0;
			console.log("Is " + num + " Divisible by 8?");
			console.log(isDivisibleBy8);

		} 		
		checkDovivisibility (8);
		checkDovivisibility (17);

		function argumentFunction (){  
			console.log("This function was passed as an argument before the message was printed");
		}

		function argumentFunctionTwo (){  
			console.log("This function was passed as an argument from the second argument function");
		} 	

		function invokeFunction(argFunction){
			console.log(argFunction);
		}

		invokeFunction(argumentFunction);
		invokeFunction(argumentFunctionTwo);

		// adding and removing the parenthesis impacts the output of JS heavily.
		// when function is used w/ parenthesis, it denotes invoking a function.
		// a function used w/o parenthesis is normally associated w/ using the function as an argument to another function.

	// Using multiple Parameters.
		// multiple "arguments" will correspond with the number of "parameters" declared in a function in "succeeding order"

			// these parameters can have default values as well.
		function createFullName (firstName, middleName, lastName){ 
			console.log("This is firstname: " + firstName);
			console.log("This is middlename: " + middleName);
			console.log("This is lastname: " + lastName);
		}

		createFullName("Rudeus", "Notus", "Greyrat");

		createFullName("Rudeus", "Notus", "Greyrat2", "Jr."); 
			// Jr. will be ignored.
		createFullName("Rudeus", "Notus Greyrat2"); 
			// if some parameters are left with no value it will show an undefined text.

		// In Js, providing more arguments than expected parameters will not create an error but will instead ignored.
		// while providing less arguments than the expected parameters will assign an undefined value to the parameter.

		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

 // [Return Statement]
 	// return statement allows us to output a value from a function to be passed to the line/block of code that invoked the function.

 	function returnFullName(firstName, middleName, lastName){
 		console.log(firstName + " " + middleName + " " + lastName);
 	}
 	returnFullName("John Rafael " , "Frias " , "Talinio");

 	function returnName(firstName, middleName, lastName){
 		return firstName + " " + middleName + " " + lastName;
 		console.log(firstName);
 	}
 	console.log(returnName("John ","Doe ","Smith"));

 	let myName = returnName("John1 ","Doe2 ","Smith3");
 	console.log(myName);

 	function printPlayerInfo(userName, level, job) {
 		console.log("Username: " + userName);
 		console.log("Level: " + level);
 		console.log("Job: " + job);
 	}
 	printPlayerInfo("Flynth", 80, "Knight_of_Rounds");